#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>

#include "HH.h"
#include "HH.max"
// #include "Maxfiles.h"
#include "MaxSLiCInterface.h"

#define WRITE_OUTPUT 1

float max(float a, float b){
	return a>b ? a : b;
}

uint32_t intCeil(uint32_t x, uint32_t y){
	uint32_t q = (x + y - 1) / y;
	return q;
}

void printDuration(struct timeval *start, struct timeval *end){
	printf ("Total time = %f seconds\n",
	         (double) (end->tv_usec - start->tv_usec) / 1000000 +
	         (double) (end->tv_sec - start->tv_sec));
}

void printDurationToFile(int64_t iteration, int64_t nGates, int64_t nCells, struct timeval *start, struct timeval *end, FILE *file){
	fprintf (file, "%d\t%d\t%d\t%f\n", iteration, nGates, nCells,
	         (double) (end->tv_usec - start->tv_usec) / 1000000 +
	         (double) (end->tv_sec - start->tv_sec));
}

void printArray(int size, float *array){
	for(int i = 0; i < size; i++){
		printf("array[%d]: %2.14f\n ", i, array[i]);
	}
}

void printArrayUInt(int size, uint32_t *array){
	for(int i = 0; i < size; i++){
		printf("array[%d]: %d\n ", i, array[i]);
	}
}

uint32_t sumArray32(int64_t size, uint32_t *array){
	int64_t i;
	uint32_t sum = 0;
	printf("size: %" PRId64 "\n", size);
	for(i = 0; i < size; i++){
		sum += array[i] & 0xFFFF;
		//printf("array[%d]: %d\n", (int) i, (int) array[i]);
	}
	return sum;
}

uint16_t sumArray16(int64_t size, uint16_t *array){
	int64_t i;
	int16_t sum = 0;
	for(i = 0; i < size; i++){
		sum += array[i];
	}
	return sum;
}

typedef struct __attribute__((__packed__)) {
	uint32_t aFtype;
	float aX1;
	float aX2;
	float aX3;
	float aX4;
	float aX5;
	float aX6;
	float aX7;
	float aX8;
	float aX9;
	uint32_t bFtype;
	float bX1;
	float bX2;
	float bX3;
	float bX4;
	float bX5;
	float bX6;
	float bX7;
	float bX8;
	float bX9;
	uint32_t p;
	float g;
	float vChannel;
	float yInit;

} ChannelConstStruct;

ChannelConstStruct newChannelConstStruct(uint32_t aFtype, float aX1, float aX2, float aX3, \
											float aX4, float aX5, float aX6, \
											float aX7, float aX8, float aX9, \
											uint32_t bFtype, float bX1, float bX2, float bX3, \
											float bX4, float bX5, float bX6, \
											float bX7, float bX8, float bX9, \
											uint32_t p, float g, float vChannel, float yInit){
	ChannelConstStruct c;
	c.aFtype = aFtype;
	c.aX1 = aX1;
	c.aX2 = aX2;
	c.aX3 = aX3;
	c.aX4 = aX4;
	c.aX5 = aX5;
	c.aX6 = aX6;
	c.aX7 = aX7;
	c.aX8 = aX8;
	c.aX9 = aX9;
	c.bFtype = bFtype;
	c.bX1 = bX1;
	c.bX2 = bX2;
	c.bX3 = bX3;
	c.bX4 = bX4;
	c.bX5 = bX5;
	c.bX6 = bX6;
	c.bX7 = bX7;
	c.bX8 = bX8;
	c.bX9 = bX9;
	c.p = p;
	c.g = g;
	c.vChannel = vChannel;
	c.yInit = yInit;

	return c;
}

typedef struct __attribute__((__packed__)) {
	uint32_t iAppStart;
	uint32_t iAppEnd;
	float iAppAmplitude;
	float S;
	float vLeak;
	float gLeak;
	uint32_t nCA;
	float gp;
} CompartmentConstStruct;

CompartmentConstStruct newCompartmentConstStruct(uint32_t iAppStart, uint32_t iAppEnd, float iAppAmplitude, float S, float vLeak, float gLeak, uint32_t nCA, float gp){
	CompartmentConstStruct c;
	c.iAppStart = iAppStart;
	c.iAppEnd = iAppEnd;
	c.iAppAmplitude = iAppAmplitude;
	c.S = S;
	c.vLeak = vLeak;
	c.gLeak = gLeak;
	c.nCA = nCA;
	c.gp = gp;

	return c;
}


void writeMultiCompInit(FILE * file, float *vs, float *ys, float dt, uint32_t nCells, uint32_t nCompartments, uint32_t *nChannels, uint32_t nSteps){
	int32_t i = 0;
	uint32_t j, k, l;
	// uint32_t totalCompartments = nCells * nCompartments;
	uint32_t nChannelsPerCell = sumArray32(nCompartments, nChannels);
	uint32_t totalChannels = nChannelsPerCell * nCells;
	fprintf(file, "ODE = fwd, nCells = %d\t, nCompartments = %d\t, Simtime = %.4f\t, nSteps = %d\t, dt = %.8f\n", nCells, nCompartments, dt * nSteps, nSteps, dt);
	fprintf(file, "vDend vSoma vAxon ysDend ysSoma ysAxon\n");
	float t = dt * i;
	for(j = 0; j < nCells; j++){
		fprintf(file, "%.8f\t%d\t ", t, j);
		printf("printV\n");
		for(k = 0; k < nCompartments; k++){
			/*if(i == 0){
				printf("v%d: %.12f\n", k, vs[i * totalCompartments + j * nCompartments + k]);
			}*/
			fprintf(file, "%.14f\t", vs[j * nCompartments + k]);
		}
		printf("printY\n");
		printf("nChannelsPerCell: %d\n", nChannelsPerCell);
		for(l = 0; l < nChannelsPerCell -1; l++){
			uint32_t yIndex = j * nChannelsPerCell + l;
			//printf("yIndex %d\n", yIndex);
			fprintf(file, "%.14f\t", ys[yIndex]);
		}
		fprintf(file, "%.14f\n", ys[i * totalChannels + j * (nChannelsPerCell) + nChannelsPerCell -1]);
	}

}

void writeMultiComp(FILE * file, float *vs, float *ys, float dt, uint32_t nCells, uint32_t nCompartments, uint32_t *nChannels, uint32_t nSteps){
	uint32_t i, j, k, l;
	float t;
	uint32_t totalCompartments = nCells * nCompartments;
	uint32_t nChannelsPerCell = sumArray32(nCompartments, nChannels);
	uint32_t totalChannels = nChannelsPerCell * nCells;
	//fprintf(file, "ODE = fwd, nCells = %d\t, nCompartments = %d\t, Simtime = %.4f\t, nSteps = %d\t, dt = %.8f\n", nCells, nCompartments, dt * nSteps, nSteps, dt);
	//fprintf(file, "vDend vSoma vAxon ysDend ysSoma ysAxon\n");
	for(j = 0; j < nCells; j++){
		for(i = 0; i < nSteps; i++){
		t = dt * (i+1);

			fprintf(file, "%.8f\t%d\t ", t, j);
			for(k = 0; k < nCompartments; k++){
				/*if(i == 0){
					printf("v%d: %.12f\n", k, vs[i * totalCompartments + j * nCompartments + k]);
				}*/
				fprintf(file, "%.14f\t", vs[i * totalCompartments + j * nCompartments + k]);
			}
			for(l = 0; l < nChannelsPerCell - 1; l++){
				uint32_t yIndex = i * totalChannels + j * nChannelsPerCell + l;
				fprintf(file, "%.14f\t", ys[yIndex]);
			}
			fprintf(file, "%.14f\n", ys[i * totalChannels + j * (nChannelsPerCell) + nChannelsPerCell -1]);
		}
	}
}

void writeToFile(FILE *file, float *vIn, float *yIn, float *vs, float *ys, float dt, uint32_t nCells, uint32_t nCompartments, uint32_t *nChannels, uint32_t nSteps, uint32_t offsetGap){
	uint32_t i, iOut, j, k, l;
	float t;
	uint32_t totalCompartments = nCells * nCompartments;
	uint32_t nChannelsPerCell = sumArray32(nCompartments, nChannels);
	uint32_t totalChannels = nChannelsPerCell * nCells;

	fprintf(file, "ODE = fwd, nCells = %d\t, nCompartments = %d\t, Simtime = %.4f\t, nSteps = %d\t, dt = %.8f\n", nCells, nCompartments, dt * nSteps, nSteps, dt);
	fprintf(file, "vDend vSoma vAxon ysDend ysSoma ysAxon\n");

	for(j = 0; j < nCells; j++){
		for(i = 0; i <= nSteps; i++){
			t = i * dt;
			fprintf(file, "%.8f\t%d\t", t, j);
			for(k = 0; k < nCompartments; k++){
				if(i == 0){
					fprintf(file, "%.12f\t", vIn[j * nCompartments + k]);
				}else{
					iOut = i - 1;
					if(k == 0){
						fprintf(file, "%.12f\t", vs[iOut * totalCompartments + (nCompartments - 1) * nCells + j]);
					}else{
						fprintf(file, "%.12f\t", vs[iOut * totalCompartments + (nCompartments - 1) * j + (k - 1)]);
					}
				}
			}

			for(l = 0; l < nChannelsPerCell - 1; l++){
				uint32_t yIndex;
				if(i == 0){
					yIndex = l;
					fprintf(file, "%.12f\t", yIn[yIndex]);
				}else{
					iOut = i - 1;
					yIndex = iOut * totalChannels + j * nChannelsPerCell + l;
					fprintf(file, "%.12f\t", ys[yIndex]);
				}
			}
			if(i == 0){
				fprintf(file, "%.12f\n", yIn[nChannelsPerCell -1]);
			}else{
				fprintf(file, "%.12f\n", ys[i * totalChannels + j * nChannelsPerCell + nChannelsPerCell -1]);
			}
		}
	}
}

void writeToFileSingle(FILE *file, float *vIn, float *yIn, float *vs, float *ys, float dt, uint32_t nCells, uint32_t nCompartments, uint32_t *nChannels, uint32_t nSteps, uint32_t cell){
	uint32_t i, iOut, j, k, l;
	float t;
	uint32_t totalCompartments = nCells * nCompartments;
	uint32_t nChannelsPerCell = sumArray32(nCompartments, nChannels);
	uint32_t totalChannels = nChannelsPerCell * nCells;

	fprintf(file, "ODE = fwd, nCells = %d\t, nCompartments = %d\t, Simtime = %.4f\t, nSteps = %d\t, dt = %.8f\n", nCells, nCompartments, dt * nSteps, nSteps, dt);
	fprintf(file, "vDend vSoma vAxon ysDend ysSoma ysAxon\n");

	j = cell;
	for(i = 0; i <= nSteps; i++){
		t = i * dt;
		fprintf(file, "%.4f\t%d\t", t, j);
		for(k = 0; k < nCompartments; k++){
			if(i == 0){
				fprintf(file, "%.8f\t", vIn[j * nCompartments + k]);
			}else{
				iOut = i - 1;
				fprintf(file, "%.8f\t", vs[iOut * totalCompartments + j * nCompartments + k]);
			}
		}
		uint32_t yIndex;
		for(l = 0; l < nChannelsPerCell - 1; l++){
			if(i == 0){
				yIndex = l;
				fprintf(file, "%.8f\t", yIn[yIndex]);
			}else{
				iOut = i - 1;
				yIndex = iOut * totalChannels + j * nChannelsPerCell + l;
				fprintf(file, "%.8f\t", ys[yIndex]);
			}
		}
		if(i == 0){
			fprintf(file, "%.8f\n", yIn[nChannelsPerCell -1]);
		}else{
			iOut = i - 1;
			yIndex = iOut * totalChannels + j * nChannelsPerCell + nChannelsPerCell - 1;
			fprintf(file, "%.8f\n", ys[yIndex]);
			// printf("yIndex: %d\n", yIndex);
		}
	}

}

void makeNChannelsPrint(int64_t nCompartments, uint32_t nPipes, uint32_t *nChannels, uint32_t *nChannelsPrint){
	int64_t i;
	for(i = 0; i < nCompartments; i++){
		uint32_t nChannelsPerCompartment = nChannels[i] & 0xFFFF;
		nChannelsPrint[i] = intCeil(nChannelsPerCompartment, nPipes) * nPipes;
	}
}

void preSumWithMin32(uint32_t size, uint32_t min, uint32_t *arrayIn, uint32_t *arrayOut){
	uint32_t i;
	arrayOut[0] = 0;
	for(i = 0; i < size; i++){
		arrayOut[i + 1] = arrayOut[i] + max(arrayIn[i] & 0xFF, min);
	}
}

void writeSingleCell(FILE *file, float *yIn, float *vIn, float *yOut, float *vOut,
						float dt, uint32_t nCells, uint32_t nCompartments, uint32_t *nChannels,
						uint32_t nSteps, uint32_t nPipes, uint32_t cell){
	//printf("start writing file\n");
	uint32_t i, j, k, l;
	float t;
	uint32_t *nChannelsPrint = malloc(nCompartments * sizeof(uint32_t));
	makeNChannelsPrint(nCompartments, nPipes, nChannels, nChannelsPrint);
	// uint32_t totalCompartments = nCells * nCompartments;
	uint32_t nChannelsPerCell = sumArray32(nCompartments, nChannels);
	uint32_t nChannelsPerCellPrint = sumArray32(nCompartments, nChannelsPrint);
	uint32_t *preSumChannels = malloc((nCompartments + 1) * sizeof(uint32_t));
	preSumWithMin32(nCompartments, nPipes, nChannelsPrint, preSumChannels);
	j = cell;

	fprintf(file, "ODE = fwd, nCells = %d\t, nCompartments = %d\t, nChannels = %d\t, Simtime = %.4f\t, nSteps = %d\t, dt = %.8f\n", nCells, nCompartments, nChannelsPerCell, dt * nSteps, nSteps, dt);
	fprintf(file, "vDend vSoma vAxon ysDend ysSoma ysAxon\n");
	for(i = 0; i <= nSteps; i++){
		t = dt * i;
		if(i == 0){
			//printf("write init\n");
			fprintf(file,"%.8f\t%d\t", t, j);
			for(k = 0; k < nCompartments; k++){
				fprintf(file, "%.12f\t", vIn[j * nCompartments + k]);
			}
			for(l = 0; l < nChannelsPerCell- 1; l++){
				fprintf(file, "%.12f\t", yIn[j * nChannelsPerCell + l]);
			}
			l = nChannelsPerCell - 1;
			fprintf(file, "%.12f\n", yIn[j * nChannelsPerCell + l]);
		}else{
			//printf("write output\n");
			uint32_t iOut = i - 1;
			fprintf(file,"%.8f\t%d\t", t, j);
			//printf("write vs\n");
			for(k = 0; k < nCompartments; k++){
				uint32_t vIndex = iOut * nCells * nCompartments + j * nCompartments + k;
				fprintf(file, "%.12f\t", vOut[vIndex]);
			}
			//printf("write ys\n");
			for(k = 0; k < nCompartments - 1; k++){
				//printf("(%d) start: %d, end:%d\n", k, preSumChannels[k], preSumChannels[k] + (nChannels[k] & 0xFF));
				for(l = preSumChannels[k]; l < preSumChannels[k] + (nChannels[k] & 0xFF); l++){
					uint32_t yIndex = iOut * nCells * nChannelsPerCellPrint + j * nChannelsPerCellPrint + l;
					//printf("yIndex: %d\n", yIndex);
					fprintf(file, "%.12f\t", yOut[yIndex]);
				}
			}
			//printf("write final y\n");
			k = nCompartments - 1;
			for(l = preSumChannels[k]; l < preSumChannels[k] + (nChannels[k] & 0xFF) - 1; l++){
				uint32_t yIndex = iOut * nCells * nChannelsPerCellPrint + j * nChannelsPerCellPrint + l;
				//printf("yIndex: %d\n", yIndex);
				fprintf(file, "%.12f\t", yOut[yIndex]);
			}
			l  = preSumChannels[k] + (nChannels[k] & 0xFF) - 1;
			uint32_t yIndex = iOut * nCells * nChannelsPerCellPrint + j * nChannelsPerCellPrint + l;
			//printf("yIndex: %d\n", yIndex);
			fprintf(file, "%.12f\n", yOut[yIndex]);
		}
	}
}


void copyChannels(int64_t sizeSrc, uint16_t *src, int64_t sizeDst, uint32_t *dst, uint16_t nChannelsPerCell){
	if((sizeDst % sizeSrc) != 0){
		printf("sizeDst(%" PRId64 ") should be a multiple of sizeSrc(%" PRId64 ")\n", sizeDst, sizeSrc);
		exit(-1);
	}
	int64_t nRepeats = sizeDst / sizeSrc;
	int64_t i, j;

	for(i = 0; i < nRepeats; i++){
		for(j = 0; j < sizeSrc; j++){
			dst[i * sizeSrc + j] = (nChannelsPerCell << 16) | (src[j]);
		}
	}
}

void checkNChannels(int64_t totalCompartments, int64_t nChannelsMax, uint32_t *nChannels){
	int64_t i;
	for(i = 0; i < totalCompartments; i++){
		if((nChannels[i] & 0xFF) > nChannelsMax){
			printf("too many channels, max = %" PRId64 "\n", nChannelsMax);
			exit(-2);
		}
	}
}

void initWs(int64_t nCells, float *ws){
	for(int64_t i = 0; i < nCells; i++){
		for(int64_t j = 0; j < nCells; j++){
			ws[i * nCells + j] = -0.0;
		}
	}
}

void initCompartments(int64_t constantNCompartments, int8_t cellType, float dt, int64_t nCells, float *vIn, CompartmentConstStruct *compartmentConstants){
	float gL = 0.3;
	float S = 1;

	float vL = 10.6;
	float vL_axon = 10;
	float vL_soma = 10;
	float vL_dend = 10;

	float gL_axon = 0.016;
	float gL_soma = 0.016;
	float gL_dend = 0.016;

	float gInt = -0.13; //0.13;
	float p1 = 0.25;
	float p2 = 0.15;

	for(int64_t i = 0; i < nCells; i++){
		float fi = i;
		// uint32_t startIApp = round(200.0 / dt);
		// uint32_t endIApp = round(250.0 / dt);
		// float amplitude = 20 * fmod(fi, 20);
		uint32_t startIApp = round(1000.0 / dt);
		uint32_t endIApp = round(1050.0 / dt);
		float amplitude = 6;
		switch(cellType){
		case 0: // HH
			vIn[i] = 0 - fi;
			compartmentConstants[i] = newCompartmentConstStruct(startIApp, endIApp, amplitude, S, vL, gL, 0, vIn[i]);
			break;
		case 1: // axon
			vIn[i] = -60;
			compartmentConstants[i] = newCompartmentConstStruct(startIApp, endIApp, amplitude, S, vL_axon, gL_axon, 0, vIn[i]);
			break;
		case 2: // soma
			vIn[i] = -60;
			compartmentConstants[i] = newCompartmentConstStruct(startIApp, endIApp, amplitude, S, vL_soma, gL_soma, 0, vIn[i]);
			break;
		case 3: // dend
			vIn[i] = -60;
			compartmentConstants[i] = newCompartmentConstStruct(startIApp, endIApp, amplitude, S, vL_dend, gL_dend, 1, vIn[i]);
			break;
		case 4: // IO
			vIn[i * constantNCompartments] = -60.0; 	// 4 * fmod(fi, 20);
			vIn[i * constantNCompartments + 1] = -60.0; // 4 * fmod(fi, 30);
			vIn[i * constantNCompartments + 2] = -60.0; // 4 * fmod(fi, 10);
			compartmentConstants[i * constantNCompartments] = newCompartmentConstStruct(startIApp, endIApp, amplitude, S, vL_dend, gL_dend, 1, gInt);
			compartmentConstants[i * constantNCompartments + 1] = newCompartmentConstStruct(startIApp, endIApp, 0.0, S, vL_soma, gL_soma, 0, p1);
			compartmentConstants[i * constantNCompartments + 2] = newCompartmentConstStruct(startIApp, endIApp, 0.0, S, vL_axon, gL_axon, 0, p2);
			break;
		}
	}
}

void initNCompartments(int64_t nCells, int64_t constantNCompartments, uint32_t *nCompartments){
	for(int64_t i = 0; i < nCells; i++){
		nCompartments[i] = constantNCompartments;
	}
}

void initGates(int64_t nChannelsPerCell, int8_t cellType, int64_t nCells, float *yIn, ChannelConstStruct *channelConstants){
	float gNa = 120;
	float gK = 36;

	float vNa = 115;
	float vK = -12;

	float gNa_a = 240;
	float vNaIO = 55;

	float gK_a = 20;
	float vKIO = -75;

	float gCal = 0.68;
	float vCA = 120;

	float gNa_s = 150;
	float gKdr_s = 9;
	float gK_s = 5;

	float gH = 0.125;
	float vHIO = -43;
	float gCaH = 4.5;
	float gKCA= 35;


	for(int64_t i = 0; i < nCells; i++){
		switch(cellType){
		case 0: // HH
			yIn[0] = 0.5; // m
			yIn[1] = 0.5; // h
			yIn[2] = 0.5; // n
			channelConstants[i * nChannelsPerCell] = newChannelConstStruct(0, 1, 25, 0.1, -1, 0, 0.1, 25, 0, 0, \
																			2, 4, 0, 1.0 / 18.0, 0, 0, 0, 0, 1, 0, \
																			3, 0 , vNa, yIn[0]);
			channelConstants[i * nChannelsPerCell + 1] = newChannelConstStruct(2, 0.07, 0, 0.05, 0, 0, 0 ,0, 1, 0,
																				2, 0, 0, 0, 1, 1, 0.1, 30, 1, 0,
																				1, gNa, vNa, yIn[1]);
			channelConstants[i * nChannelsPerCell + 2] = newChannelConstStruct(0, 1, 10, 0.1, -1, 0, 0.01, 10, 0, 0,
																				2, 0.125, 0, 0.0125, 0, 0, 0 ,0 ,1 ,0,
																				4, gK, vK, yIn[2]);
			break;
		case 1: // axon
			yIn[0] = 0.0;
			yIn[1] = 0.9;
			yIn[2] = 0.2369847;
			channelConstants[i * nChannelsPerCell] = newChannelConstStruct(2, 0, 0, 0, 1, 0, 0, 0, 1, 0,
																			14, 0, 0, 0 , 1, 1 ,1.0 / 5.5 , -30, 1, 0,
																			3, 0 , vNaIO, yIn[0]); // m
			channelConstants[i * nChannelsPerCell + 1] = newChannelConstStruct(6, 0, 0, 0, 1 , 1, -1.0 / 5.8, -60, 1, 0,
																				6, 1.5, -40, 1.0 / 33.0, 0, 0, 0 , 0, 1, 0,
																				1, gNa_a, vNaIO, yIn[1]); // h
			channelConstants[i * nChannelsPerCell + 2] = newChannelConstStruct(0, -1, -25, 0.1, 1, 0, -0.13, -25, 0, 0,
																				2, 1.69, -35, 0.0125, 0, 0, 0, 0, 1, 0,
																				4, gK_a, vKIO, yIn[2]); // x
			break;
		case 2: // soma
			yIn[0] = 0.7423;
			yIn[1] = 0.0321349;
			yIn[2] = 0.0;
			yIn[3] = 0.3596066;
			yIn[4] = 0.2369847;
			yIn[5] = 0.1;
			channelConstants[i * nChannelsPerCell] = newChannelConstStruct(6, 0, 0, 0, 1, 1, 1.0/4.2, -61, 1, 0,
																			6, 0, 0, 0, 0, 0, 0 , 0 , 1, 1,
																			3, 0, vCA, yIn[0]); // k
			channelConstants[i * nChannelsPerCell + 1] = newChannelConstStruct(6, 0, 0, 0, 1, 1, -1.0 / 8.5, -85.5, 1, 0,
																				6, 20, -160, -1.0 / 30.0, 0, 1, -1.0 / 7.3, -84, 1, 35,
																				1, gCal, vCA, yIn[1]); // l
			channelConstants[i * nChannelsPerCell + 2] = newChannelConstStruct(2, 0, 0, 0, 1, 0, 0, 0, 1, 0,
																				14, 0, 0, 0, 1, 1 , 1.0 / 5.5, -30, 1, 0,
																				3, 0 , vNaIO, yIn[2]); // m
			channelConstants[i * nChannelsPerCell + 3] = newChannelConstStruct(6, 0, 0, 0, 1, 1, -1.0 / 5.8, -70, 1, 0,
																				6, 3, -40, 1.0 / 33, 0, 0, 0, 0, 1, 0,
																				1, gNa_s, vNaIO, yIn[3]); // h
			channelConstants[i * nChannelsPerCell + 4] = newChannelConstStruct(6, 0, 0, 0, 1, 1, 0.1, -3, 1, 0,
																				6, 47, -50, -1.0 / 900.0, 5, 0 ,0 ,0 ,1, 0,
																				4, gKdr_s, vKIO, yIn[4]); // n
			channelConstants[i * nChannelsPerCell + 5] = newChannelConstStruct(0, -1, -25, 0.1, 1, 0, -0.13, -25, 0, 0,
																				2, 1.69, -35, 0.0125, 0, 0, 0, 0, 1, 0,
																				4, gK_s, vKIO, yIn[5]); // x
			break;
		case 3: // dend
			yIn[0] = 0.0112788;
			yIn[1] = 3.7152;
			yIn[2] = 0.0049291;
			yIn[3] = 0.0337836;
			channelConstants[i * nChannelsPerCell] = newChannelConstStruct(2, 0, 0, 0, 0.34, 1, 1.0 / 13.9, 5, 1, 0,
																				0, -1, -8.5, -0.2, 1, 0, (0.1 * 0.2) / 5.0, -8.5, 0, 0,
																				2, gCaH, vCA, yIn[0]); // r

			channelConstants[i * nChannelsPerCell + 1] = newChannelConstStruct(10, 0, 0, 0, 0, 0, 0, 0, 1, -3,
																				10, 0, 0, 0, 0, 0, 0, 0, 1, 0.075,
																				1, 0, vKIO, yIn[1]); // caconc

			channelConstants[i * nChannelsPerCell + 2] = newChannelConstStruct(3, 0.01, 0, -0.00002, 0, 0, 0, 0, 0, 0,
																				2, 0, 0, 0, 0, 0, 0, 0, 1, 0.015,
																				1, gKCA, vKIO, yIn[2]); // s

			channelConstants[i * nChannelsPerCell + 3] = newChannelConstStruct(6, 0, 0, 0, 1, 1, -0.25, -80, 1, 0,
																				1, 1, -14.6 / 0.086, 0.086, 0, 1, -0.07, 1.87 / 0.07, 0, 1,
																				1, gH, vHIO, yIn[3]); // q
			break;
		case 4: //IO
			// dend
			yIn[0] = 0.0112788;
			yIn[1] = 3.7152;
			yIn[2] = 0.0049291;
			yIn[3] = 0.0337836;
			channelConstants[i * nChannelsPerCell] = newChannelConstStruct(2, 0, 0, 0, 0.34, 1, 1.0 / 13.9, 5, 1, 0,
																				0, -1, -8.5, -0.2, 1, 0, (0.1 * 0.2) / 5.0, -8.5, 0, 0,
																				2, gCaH, vCA, yIn[0]); // r
			channelConstants[i * nChannelsPerCell + 1] = newChannelConstStruct(10, 0, 0, 0, 0, 0, 0, 0, 1, 3,
																				10, 0, 0, 0, 0, 0, 0, 0, 1, 0.075,
																				1, 0, vKIO, yIn[1]); // caconc
			channelConstants[i * nChannelsPerCell + 2] = newChannelConstStruct(3, 0.01, 0, -0.00002, 0, 0, 0, 0, 0, 0,
																				2, 0, 0, 0, 0, 0, 0, 0, 1, 0.015,
																				1, gKCA, vKIO, yIn[2]); // s
			channelConstants[i * nChannelsPerCell + 3] = newChannelConstStruct(6, 0, 0, 0, 1, 1, -0.25, -80, 1, 0,
																			1, 1, -14.6 / 0.086, 0.086, 0, 1, -0.07, 1.87 / 0.07, 0, 1,
																			1, gH, vHIO, yIn[3]); // q
			// soma
			int64_t somaOffset = 4;
			yIn[0 + somaOffset] = 0.7423159;
			yIn[1 + somaOffset] = 0.0321349;
			yIn[2 + somaOffset] = 1.0127807; // m was 0
			yIn[3 + somaOffset] = 0.3596066;
			yIn[4 + somaOffset] = 0.2369847;
			yIn[5 + somaOffset] = 0.1;
			channelConstants[i * nChannelsPerCell + somaOffset] = newChannelConstStruct(6, 0, 0, 0, 1, 1, 1.0/4.2, -61, 1, 0,
																			6, 0, 0, 0, 0, 0, 0 , 0 , 1, 1,
																			3, 0, vCA, yIn[0 + somaOffset]); // k
			channelConstants[i * nChannelsPerCell + 1 + somaOffset] = newChannelConstStruct(6, 0, 0, 0, 1, 1, -1.0 / 8.5, -85.5, 1, 0,
																				6, 20, -160, -1.0 / 30.0, 0, 1, -1.0 / 7.3, -84, 1, 35,
																				1, gCal, vCA, yIn[1 + somaOffset]); // l
			channelConstants[i * nChannelsPerCell + 2 + somaOffset] = newChannelConstStruct(2, 0, 0, 0, 1, 0, 0, 0, 1, 0,
																				14, 0, 0, 0, 1, 1 , 1.0 / 5.5, -30, 1, 0,
																				3, 0 , vNaIO, yIn[2 + somaOffset]); // m
			channelConstants[i * nChannelsPerCell + 3 + somaOffset] = newChannelConstStruct(6, 0, 0, 0, 1, 1, -1.0 / 5.8, -70, 1, 0,
																				6, 3, -40, 1.0 / 33, 0, 0, 0, 0, 1, 0,
																				1, gNa_s, vNaIO, yIn[3 + somaOffset]); // h
			channelConstants[i * nChannelsPerCell + 4 + somaOffset] = newChannelConstStruct(6, 0, 0, 0, 1, 1, 0.1, -3, 1, 0,
																				6, 47, -50, -1.0 / 900.0, 5, 0 ,0 ,0 ,1, 0,
																				4, gKdr_s, vKIO, yIn[4 + somaOffset]); // n
			channelConstants[i * nChannelsPerCell + 5 + somaOffset] = newChannelConstStruct(0, -1, -25, 0.1, 1, 0, -0.13, -25, 0, 0,
																				2, 1.69, -35, 0.0125, 0, 0, 0, 0, 1, 0,
																				4, gK_s, vKIO, yIn[5 + somaOffset]); // x

			// axon
			int64_t axonOffset = 6 + somaOffset;
			yIn[0 + axonOffset] = 0.0;
			yIn[1 + axonOffset] = 0.9;
			yIn[2 + axonOffset] = 0.2369847;
			channelConstants[i * nChannelsPerCell + axonOffset] = newChannelConstStruct(2, 0, 0, 0, 1, 0, 0, 0, 1, 0,
																			14, 0, 0, 0 , 1, 1 ,1.0 / 5.5 , -30, 1, 0,
																			3, 0 , vNaIO, yIn[0 + axonOffset]); // m
			channelConstants[i * nChannelsPerCell + 1 + axonOffset] = newChannelConstStruct(6, 0, 0, 0, 1 , 1, -1.0 / 5.8, -60, 1, 0,
																				6, 1.5, -40, 1.0 / 33.0, 0, 0, 0 , 0, 1, 0,
																				1, gNa_a, vNaIO, yIn[1 + axonOffset]); // h
			channelConstants[i * nChannelsPerCell + 2 + axonOffset] = newChannelConstStruct(0, -1, -25, 0.1, 1, 0, -0.13, -25, 0, 0,
																				2, 1.69, -35, 0.0125, 0, 0, 0, 0, 1, 0,
																				4, gK_a, vKIO, yIn[2 + axonOffset]); // x
			break;
		}
	}
}

void initGatesPerPipe(uint32_t nPipes, uint32_t nCompartments, uint32_t *nChannels, int64_t *nChannelsPerPipe, ChannelConstStruct *channelConstants, int64_t *sizeChannelsPerPipe, ChannelConstStruct **channelConstantsPerPipe){
	uint32_t k = 0;
	uint32_t *kPerPipe = malloc(nPipes * sizeof(uint32_t));

	for(uint32_t i = 0; i < nPipes; i++){
		sizeChannelsPerPipe[i] = nChannelsPerPipe[i] * sizeof(ChannelConstStruct);
		channelConstantsPerPipe[i] = malloc(sizeChannelsPerPipe[i]);
		kPerPipe[i] = 0;
	}

	for(uint32_t i = 0; i < nCompartments; i++){
		for(uint32_t j = 0; j < (nChannels[i] & 0xFFFF); j++){
			channelConstantsPerPipe[j % nPipes][kPerPipe[j % nPipes]++] = channelConstants[k++];
		}
	}

	free(kPerPipe);
}

int64_t setConstantNCompartments(int8_t cellType){
	int64_t constantNCompartments;
	switch(cellType){
		case 0:
			printf("all HH cells\n");
			constantNCompartments = 1;
			break;
		case 1:
			printf("all axon cells\n");
			constantNCompartments = 1;
			break;
		case 2:
			printf("all soma cells\n");
			constantNCompartments = 1;
			break;
		case 3:
			printf("all dend cells\n");
			constantNCompartments = 1;
			break;
		case 4:
			printf("all IO cells\n");
			constantNCompartments = 3;
			break;
		default:
			printf("cellType not supported");
			exit(-1);
			break;
	}
	return constantNCompartments;
}

void setNChannelsPerCompartment(int8_t cellType, uint16_t *nChannelsPerCompartment){
	switch(cellType){
		case 0:
			nChannelsPerCompartment[0] = 3;
			break;
		case 1:
			nChannelsPerCompartment[0] = 3;
			break;
		case 2:
			nChannelsPerCompartment[0] = 6;
			break;
		case 3:
			nChannelsPerCompartment[0] = 4;
			break;
		case 4:
			nChannelsPerCompartment[0] = 4;
			nChannelsPerCompartment[1] = 6;
			nChannelsPerCompartment[2] = 3;
			break;
	}
}

void initNChannelsPerPipe(uint32_t nPipes, uint32_t nCells, uint32_t *nChannels, int64_t *nChannelsPerPipe){
	for(uint32_t j = 0; j < nPipes; j++){
		nChannelsPerPipe[j] = 0;
	}

	for(uint32_t i = 0; i < nCells; i++){
		uint32_t n = nChannels[i] & 0xFF;
		int64_t nPerPipeMin = n / nPipes;
		for(uint32_t j = 0; j < nPipes; j++){
			nChannelsPerPipe[j] += nPerPipeMin + ((n % nPipes) > j);
		}
	}
}

void writeToFileSingleCompartment(FILE *file, float *vIn, float *yIn, float *vs, float *ys, float dt, uint32_t nCells, uint32_t *nChannels, uint32_t nSteps, uint32_t cell, uint32_t nPipes){
	float t;
	uint32_t i, iOut, j;
	uint32_t nChannelsPerCell = sumArray32(1, nChannels);
	fprintf(file, "ODE = fwd, nCells = %d\t, Simtime = %.4f\t, nSteps = %d\t, dt = %.8f\n", nCells, dt * nSteps, nSteps, dt);
	fprintf(file, "time cell v ys\n");
	uint32_t nChannelsOut = ((nChannelsPerCell + nPipes - 1) / nPipes) * nPipes;
	for(i = 0; i <= nSteps; i++){
		t = i * dt;
		fprintf(file, "%.4f\t%d\t", t, cell);
		if(i == 0){
			fprintf(file, "%.8f\t", vIn[cell]);
			for(j = 0; j < nChannelsPerCell - 1; j++){
				fprintf(file, "%.8f\t", yIn[j]);
			}
			fprintf(file, "%.8f\n", yIn[nChannelsPerCell - 1]);
		}else{
			iOut = i - 1;
			fprintf(file, "%.8f\t", vs[iOut * nCells + cell]);
			printf("v[%d]: %.8f\n", iOut * nCells + cell, vs[iOut * nCells + cell]);
			for(j = 0; j < nChannelsPerCell - 1; j++){
				fprintf(file, "%.8f\t", ys[iOut * nCells * nChannelsOut + cell * nChannelsOut + j]);
				printf("y[%d]: %.8f\n", iOut * nCells * nChannelsOut + cell * nChannelsOut + j, ys[iOut * nCells * nChannelsOut + cell * nChannelsOut + j]);
			}
			fprintf(file, "%.8f\n", ys[iOut * nCells * nChannelsOut + cell * nChannelsOut + nChannelsPerCell - 1]);
		}
	}
}

void writeFile(int8_t cellType, float *vIn, float *yIn, float *vOut, float *yOut, float dt, uint32_t nCells, uint32_t nCompartments, uint32_t *nChannels, uint32_t nSteps, uint32_t nPipes){
	uint32_t offsetGap = 0;
	FILE *refFile;
	FILE *refFile2;
	char buff[100];
	char buff2[100];
	uint32_t cell = 0;
	uint32_t cell2 = 105;
	switch(cellType){
	case 0:
		refFile = fopen("../../doc/HHmultiDFEDebug.dat", "w");
		break;
	case 1:
		refFile = fopen("../../doc/HHmultiAxonDFE.dat", "w");
		break;
	case 2:
		refFile = fopen("../../doc/HHmultiSomaDFE.dat", "w");
		break;
	case 3:
		refFile = fopen("../../doc/HHmultiDendDFE.dat", "w");
		break;
	case 4:
		snprintf(buff, sizeof(buff), "../../doc/HHmultifwd_DFE%d.dat", cell);
		snprintf(buff2, sizeof(buff2), "../../doc/HHmultifwd_DFE%d.dat", cell2);
		refFile = fopen(buff, "w");
		refFile2 = fopen(buff2, "w");
		break;
	default:
		printf("printing cellType not supported refFile");
		exit(-1);
		break;
	}
	if(refFile == NULL){
		printf("could not open file, exit");
		exit(-2);
	}
	printf("opened file\n");
	if(cellType != 4){
		writeToFileSingleCompartment(refFile, vIn, yIn, vOut, yOut, dt, nCells, nChannels, nSteps, cell, nPipes);
	}else{
		uint32_t offsetGap = 0;
		writeToFileSingle(refFile, vIn, yIn, vOut, yOut, dt, nCells, nCompartments, nChannels, nSteps, cell);
		writeToFileSingle(refFile2, vIn, yIn, vOut, yOut, dt, nCells, nCompartments, nChannels, nSteps, cell2);
		fclose(refFile2);
	}
	fclose(refFile);
}

int main(void){
	printf("begin program\n");
	struct timeval start, end;

	// set simulation parameters
	float dt = 0.01;
	float simTime = 2000.0;
	const int vectorSize = 2;
	int64_t nCellsLimit = 96 * 5;
	int64_t nSteps = simTime / dt;

	int64_t maxLMemSize = 48 * pow(10, 9);
	int64_t throwAwayFactor = 1;

	int64_t nCompartmentsMax = HH_nCompartmentsMax;
	int64_t nChannelsMax = HH_nChannelsMax;
	int64_t nPipes = HH_nPipes;

	max_file_t *maxfile = HH_init();


	int64_t nCellMin = nCellsLimit;
	uint32_t maxIterations = 1;

	int64_t burstSizeInBytes = max_get_burst_size(maxfile, NULL);// 384
	int64_t burstSizeInFloats = burstSizeInBytes / sizeof(float); //  96

	printf("loopLength: %" PRId64 "\n", HH_get_HHKernel_loopLength());
	printf("burstSizeInBytes: %" PRId64 "\n", burstSizeInBytes);

	for(int64_t nCell = nCellMin; nCell <= nCellsLimit; nCell = nCell + 960){
		for(uint32_t iteration = 0; iteration < maxIterations; iteration++){ // iterations	
			int64_t nCells = nCell;
			int8_t cellType = 4;
			uint16_t tempGapAddress = 0;
			int64_t constantNCompartments = setConstantNCompartments(cellType);

			int64_t totalCompartments = nCell * constantNCompartments;
			int64_t sizeNChannels = totalCompartments * sizeof(uint32_t);

			uint32_t *nChannels = malloc(sizeNChannels);
			uint16_t *nChannelsPerCompartment = malloc(constantNCompartments * sizeof(uint16_t));

			setNChannelsPerCompartment(cellType, nChannelsPerCompartment);
			uint16_t constantNChannelsPerCell = sumArray16(constantNCompartments, nChannelsPerCompartment);
			printf("iteration =%d, nChannel =%d, nCell = %d\n", iteration, constantNChannelsPerCell, nCell);

			copyChannels(constantNCompartments, nChannelsPerCompartment, totalCompartments, nChannels, constantNChannelsPerCell);
			for(int64_t i = 0; i < totalCompartments; i++){
				if(((nChannels[i] & 0xFFFF0000) == 0) || ((nChannels[i] & 0xFFFF) == 0)){
					printf("something went wrong in copy\n");
					printf("nChannelsLast[%d]: %d\n", i, (nChannels[i] & 0x0000FFFF));
					printf("nChannelsBegin[%d]: %d\n", i, (nChannels[i] & 0xFFFF0000) >> 16);
					exit(-1);
				}
			}
			int64_t nChannelsPerCell = constantNChannelsPerCell;

			if(totalCompartments > nCompartmentsMax){
				printf("too many compartments, max = %" PRId64 "\n", nCompartmentsMax);
				exit(-2);
			}
			checkNChannels(totalCompartments, nChannelsMax, nChannels);
			
			int64_t totalChannels = 0;
			for(int64_t i = 0; i < totalCompartments; i++){
				totalChannels += (nChannels[i] & 0xFFFF);
			}

			int64_t *nChannelsPerPipe = malloc(nPipes * sizeof(int64_t));
			initNChannelsPerPipe(nPipes, totalCompartments, nChannels, nChannelsPerPipe);

			int64_t sizeChannelConstants = totalChannels * sizeof(ChannelConstStruct);
			int64_t sizeCompartmentConstants = totalCompartments * sizeof(CompartmentConstStruct);
			int64_t sizeVIn = totalCompartments * sizeof(float);
			int64_t sizeNCompartments = nCells * 2 * sizeof(uint16_t);
			int64_t totalOutChannels = nChannelsPerPipe[0] * nPipes * nSteps;
			int64_t sizeYOut = totalOutChannels * sizeof(float);
			int64_t sizeVOut = (nSteps * totalCompartments) * sizeof(float);

			ChannelConstStruct *channelConstants = malloc(sizeChannelConstants);
			CompartmentConstStruct *compartmentConstants = malloc(sizeCompartmentConstants);
			float *vIn = malloc(sizeVIn);
			uint32_t *nCompartments = malloc(sizeNCompartments);
			float *yOut = malloc(sizeYOut);
			float *vOut = malloc(sizeVOut);

			int64_t addressChannelConst = 0;
			int64_t addressCompartmentConst = addressChannelConst + sizeChannelConstants;
			int64_t addressNChannels = addressCompartmentConst + sizeCompartmentConstants;
			int64_t addressVIn = addressNChannels + sizeNChannels;
			int64_t addressNCompartments = addressVIn + sizeVIn;
			int64_t addressYOut = addressNCompartments + sizeNCompartments;
			int64_t addressVOut = addressYOut + sizeYOut;

			int64_t inputLMemSize = addressYOut;
			int64_t sizeLMemLeft = maxLMemSize - inputLMemSize;

			if(sizeYOut + sizeVOut > sizeLMemLeft){
				throwAwayFactor = 1 + (((sizeYOut + sizeVOut) - 1) / sizeLMemLeft);
				printf("throwAwayFactor is set due to shortage of LMem\n");
			}

			float *yIn = malloc(nChannelsPerCell * sizeof(float));

			initGates(nChannelsPerCell, cellType, nCells, yIn, channelConstants);
			initCompartments(constantNCompartments, cellType, dt, nCells, vIn, compartmentConstants);
			initNCompartments(nCells, constantNCompartments, nCompartments);

			int64_t *sizeChannelsPerPipe = malloc(nPipes * sizeof(int64_t));
			ChannelConstStruct *channelConstantsPerPipe[nPipes];
			initGatesPerPipe(nPipes, totalCompartments, nChannels, nChannelsPerPipe, channelConstants, sizeChannelsPerPipe, channelConstantsPerPipe);

			int64_t totalLMemSize = sizeChannelConstants + sizeCompartmentConstants + sizeNChannels + sizeVIn + sizeNCompartments;
			if(totalLMemSize > 48 * pow(10, 9)){
				printf("Input size too big for LMem\n");
				return(-1);
			}

			printf("writeLMem\n");
			int64_t addressTemp = 0;
			for(uint32_t i = 0; i < nPipes; i++){
				if(i == 0){
					addressTemp = 0;
				}else{
					addressTemp += sizeChannelsPerPipe[i - 1];
				}
				HH_writeLMem(sizeChannelsPerPipe[i], addressTemp, channelConstantsPerPipe[i]);
			}

			HH_writeLMem(sizeCompartmentConstants, addressCompartmentConst, compartmentConstants);
			HH_writeLMem(sizeNChannels, addressNChannels, nChannels);
			HH_writeLMem(sizeVIn, addressVIn, vIn);

			HH_writeLMem(sizeNCompartments, addressNCompartments, nCompartments);
			
			printf("run Kernel\n");
			gettimeofday(&start, NULL);
			HH(dt, nCells, nSteps, throwAwayFactor, totalCompartments, nChannelsPerPipe);
			gettimeofday(&end, NULL);

			printf("read LMem\n");
			HH_readLMem(sizeYOut, addressYOut, yOut);
			HH_readLMem(sizeVOut, addressVOut, vOut);

			if(WRITE_OUTPUT){
				printf("writing file\n");
				writeFile(cellType, vIn, yIn, vOut, yOut, dt, nCells, constantNCompartments, nChannels, nSteps, nPipes);
			}

			free(vIn);
			free(yIn);
			free(yOut);
			free(vOut);
			free(channelConstants);
			free(compartmentConstants);
			free(nChannels);
			free(nCompartments);
		}
	}
	
	return 0;
}
