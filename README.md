# Introduction
This repository contains the code of flexHH: A flexible hardware library for Hodgkin-Huxley-based neural simulations. For an discussion and evaluation of the code see the thesis related to this code: https://repository.tudelft.nl/islandora/object/uuid%3A84792c80-c0b8-489c-80f1-4436ed961844. 

The code is developed to run on a Maxeler DFE using the v1 Maxeler tools and MAIA DFEs.

# Overview
This library simulates Hodgkin-Huxley (HH) based models. Besides the standard HH model, the crucial extensions were added: User-defined ion gates, multi-compartmental cells and gap-junction interconnectivity. 

The table shows which features are supported by each project. (Y = Yes, N = No)

| Project   | Custom  ion gates | Gap  junctions | Multiple cell Compartments |
|----------|-------------------|----------------|----------------------------|
| HHu      | N                 | N              | N                          |
| HHgap    | N                 | Y              | N                          |
| HHcu     | Y                 | N              | N                          |
| HHmcu    | Y                 | N              | Y                          |
| HHioGenu | Y                 | Y              | Y                          |

Each project folder contains:
* CPUCode - folder containing the code for the CPU host and thus the code were the parameters/variables of the simulations are set.
* EngineCode - folder containing the code of kernels and related managers. The hardware configuration options (uf, nCompsMax, nGatesMax) are set in the code of the manager.
* RunRules - folder containing setting/ rules for compilation/ execuation of the code.


