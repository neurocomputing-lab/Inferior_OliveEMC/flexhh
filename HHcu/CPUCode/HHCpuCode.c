#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>

#include "HH.h"
#include "HH.max"
// #include "Maxfiles.h"
#include "MaxSLiCInterface.h"

/**
 *  Variables to alter behaviour of the code
 * WRITE_OUTPUT defines if all output should be written to a file
 * WRITE_SINGLE defines if the output of a single cell should be written to a file
 **/

#define WRITE_OUTPUT 0
#define WRITE_SINGLE 1

void printDuration(struct timeval *start, struct timeval *end){
	printf ("Total time = %f seconds\n",
	         (double) (end->tv_usec - start->tv_usec) / 1000000 +
	         (double) (end->tv_sec - start->tv_sec));
}

void printDurationToFile(struct timeval *start, struct timeval *end, FILE *file, uint32_t cells, uint32_t steps){
	fprintf (file, "%d\t%d\t%f\n", cells, steps,
	         (double) (end->tv_usec - start->tv_usec) / 1000000 +
	         (double) (end->tv_sec - start->tv_sec));
}

void printArray(int size, float *array){
	for(int i = 0; i < size; i++){
		printf("array[%d]: %2.14f\n ", i, array[i]);
	}
}

void printArrayUInt(int size, uint32_t *array){
	for(int i = 0; i < size; i++){
		printf("array[%d]: %d\n ", i, array[i]);
	}
}

// structure containing the variables per gate/channel
typedef struct __attribute__((__packed__)) {
	uint32_t aFtype;
	float aX1;
	float aX2;
	float aX3;
	float aX4;
	float aX5;
	float aX6;
	float aX7;
	float aX8;
	float aX9;
	uint32_t bFtype;
	float bX1;
	float bX2;
	float bX3;
	float bX4;
	float bX5;
	float bX6;
	float bX7;
	float bX8;
	float bX9;
	uint32_t p;
	float g;
	float vChannel;
	float yInit;

} ChannelConstStruct;

ChannelConstStruct newChannelConstStruct(uint32_t aFtype, float aX1, float aX2, float aX3, \
											float aX4, float aX5, float aX6, \
											float aX7, float aX8, float aX9, \
											uint32_t bFtype, float bX1, float bX2, float bX3, \
											float bX4, float bX5, float bX6, \
											float bX7, float bX8, float bX9, \
											uint32_t p, float g, float vChannel, float yInit){
	ChannelConstStruct c;
	c.aFtype = aFtype;
	c.aX1 = aX1;
	c.aX2 = aX2;
	c.aX3 = aX3;
	c.aX4 = aX4;
	c.aX5 = aX5;
	c.aX6 = aX6;
	c.aX7 = aX7;
	c.aX8 = aX8;
	c.aX9 = aX9;
	c.bFtype = bFtype;
	c.bX1 = bX1;
	c.bX2 = bX2;
	c.bX3 = bX3;
	c.bX4 = bX4;
	c.bX5 = bX5;
	c.bX6 = bX6;
	c.bX7 = bX7;
	c.bX8 = bX8;
	c.bX9 = bX9;
	c.p = p;
	c.g = g;
	c.vChannel = vChannel;
	c.yInit = yInit;

	return c;
}
// structure containing the variables per compartment/cell
typedef struct __attribute__((__packed__)) {
	uint32_t iAppStart;
	uint32_t iAppEnd;
	float iAppAmplitude;
	float S;
	float vLeak;
	float gLeak;
	uint32_t nCA;
	float vInit;
} CellConstStruct;

CellConstStruct newCellConstStruct(uint32_t iAppStart, uint32_t iAppEnd, float iAppAmplitude, float S, float vLeak, float gLeak, uint32_t nCA, float vInit){
	CellConstStruct c;
	c.iAppStart = iAppStart;
	c.iAppEnd = iAppEnd;
	c.iAppAmplitude = iAppAmplitude;
	c.S = S;
	c.vLeak = vLeak;
	c.gLeak = gLeak;
	c.nCA = nCA;
	c.vInit = vInit;

	return c;
}


void writeSingleCell(FILE * file, float *yIn, float *vIn, float *yOut, float *vOut, float dt, uint32_t nCells, uint32_t nChannels, uint32_t nSteps, uint32_t cell, uint32_t nPipes){
	float t;
	fprintf(file, "ODE = fwd, nCells = %d\t, nChannels = %d\t, Simtime = %.4f\t, nSteps = %d\t, dt = %.8f\n", nCells, nChannels, dt * nSteps, nSteps, dt);
	fprintf(file, "time (s)\t cell\t v(mV)\t\t\tm\t\t\th\t\t\tn\n");
	uint32_t j = cell;
	uint32_t nChannelsOut =  ((nChannels + nPipes - 1) / nPipes) * nPipes;
	for(uint32_t i = 0; i <= nSteps; i++){
		t = dt * i;
		if(i == 0){
			fprintf(file,"%.8f\t%d\t%.14f\t", t, j, vIn[j]);
			for(uint32_t k = 0; k < nChannels - 1; k++){
				fprintf(file, "%.14f\t", yIn[k]);
			}
			uint32_t k = nChannels - 1;
			fprintf(file, "%.14f\n", yIn[k]);
		}else{
			uint32_t iOut = i - 1;
			fprintf(file,"%.8f\t%d\t%.14f\t", t, j, vOut[iOut*nCells + j]);
			for(uint32_t k = 0; k < nChannels - 1; k++){
				fprintf(file, "%.14f\t", yOut[iOut * nChannelsOut * nCells + j * nChannelsOut + k]);
			}
			uint32_t k = nChannels - 1;
			fprintf(file, "%.14f\n", yOut[iOut * nChannelsOut * nCells + j * nChannelsOut + k]);
		}
	}
}

void writeToFile(FILE * file, float *yOut, float *vOut, float dt, uint32_t nCells, uint32_t nChannels, uint32_t nSteps){
	float t;
	for(uint32_t i = 0; i < nSteps; i++){
		for(uint32_t j = 0; j < 1; j++){
			t = dt * (i+1);
			fprintf(file,"%.8f\t%d\t%.14f\t", t, j, vOut[i*nCells + j]);
			for(uint32_t k = 0; k < nChannels - 1; k++){
				fprintf(file, "%.14f\t", yOut[i * nChannels * nCells + j * nChannels + k]);
			}
			uint32_t k = nChannels - 1;
			fprintf(file, "%.14f\n", yOut[i * nChannels * nCells + j * nChannels + k]);
		}
	}
}

void writeInitToFile(FILE * file, float *yIn, float *vIn, float dt, uint32_t nCells, uint32_t nChannels, uint32_t nSteps){
	float t;
	int i = 0;
	fprintf(file, "ODE = mod, nCells = %d\t, nChannels = %d\t, Simtime = %.4f\t, nSteps = %d\t, dt = %.8f\n", nCells, nChannels, dt * nSteps, nSteps, dt);
	fprintf(file, "time (s)\t cell\t v(mV)\t\t\tm\t\t\th\t\t\tn\n");
	for(uint32_t j = 0; j < 1; j++){
		t = dt * (i);
		fprintf(file,"%.8f\t%d\t%.14f\t", t, j, vIn[j]);
		for(uint32_t k = 0; k < nChannels - 1; k++){
			fprintf(file, "%.14f\t", yIn[j * nChannels + k]);
		}
		uint32_t k = nChannels - 1;
		fprintf(file, "%.14f\n", yIn[j * nChannels + k]);
	}

}

int main(void){
	struct timeval start, end;

	// set simulation parameters
	float dt = 0.001;
	float simTime = 20.000;
	const int vectorSize = 2;
	int64_t nCells = 96 * 10;
	int64_t nSteps = simTime / dt;

	// set memory parameters
	int64_t maxLMemSize = 48 * pow(10, 9);
	int64_t throwAwayFactor = 1;

	// retreive constants from kernel, NOTE, nPipes is the unroll factor
	int64_t nCellsMax = HH_nCellsMax;
	int64_t nChannelsMax = HH_nChannelsMax;
	int64_t nPipes = HH_nPipes;
	printf("nCellsMax %" PRId64 "\n", nCellsMax);
	printf("nChannelsMax %" PRId64 "\n", nChannelsMax);
	printf("nPipes %" PRId64 "\n", nPipes);

	/**
	 * cellType select type of cell is used
	 * 0 = all HH cells
	 * 1 = all axon cells
	 * 2 = all soma cells
	 * 3 = all dend cells
	 * 4 = axon, soma, dend cells
	 * 5 = HH, axon, soma, dend cells
	 */
	int8_t cellType = 2;
	uint32_t constantNChannels;
	switch(cellType){
	case 0:
		printf("all HH cells\n");
		constantNChannels = 3;
		break;
	case 1:
		printf("all axon cells\n");
		constantNChannels = 3;
		break;
	case 2:
		printf("all soma cells\n");
		constantNChannels = 6;
		break;
	case 3:
		printf("all dend cells\n");
		constantNChannels = 4;
		break;
	default:
		printf("cellType not supported");
		exit(-1);
		break;
	}

	// define parameters of neurons
	float gL = 0.3;
	//float C =1;
	float gNa = 120;
	float gK = 36;

	float vNa = 115;
	float vK = -12;
	float vL = 10.6;

	float gNa_a = 240;
	float vNaIO = 55;

	float gK_a = 20;
	float vKIO = -75;

	float gCal = 0.68;
	float vCA = 120;

	float gNa_s = 150;
	float gKdr_s = 9;
	float gK_s = 5;

	float gH = 0.125;
	float vHIO = -43;
	float gCaH = 4.5;
	float gKCA= 35;

	float vL_axon = 10;
	float vL_soma = 10;
	float vL_dend = 10;

	float gL_axon = 0.016;
	float gL_soma = 0.016;
	float gL_dend = 0.016;


	if(nCells > nCellsMax){
		printf("too many cells, max = %d\n", nCellsMax);
		exit(-2);
	}
	if(constantNChannels > nChannelsMax){
		printf("too many channels, max = %d\n", nChannelsMax);
		exit(-2);
	}

	int64_t size32Stream = nCells * sizeof(uint32_t);

	// prepare number of channels/gates for streams

	uint32_t *channelsPerCell = malloc(size32Stream);
	for(uint32_t i = 0; i < nCells; i++){
		channelsPerCell[i] = constantNChannels;
	}

	uint32_t *nChannels = malloc(size32Stream);
	int64_t *nChannelsPerPipe = malloc(nPipes * sizeof(int64_t));

	int64_t totalChannels = 0;
	for(int64_t i = 0; i < nCells; i++){
		totalChannels += channelsPerCell[i];
		nChannels[i] = channelsPerCell[i];
	}

	for(uint32_t j = 0; j < nPipes; j++){
		nChannelsPerPipe[j] = 0;
	}

	for(uint32_t i = 0; i < nCells; i++){
		uint32_t n = nChannels[i];
		int64_t nPerPipeMin = n / nPipes;
		for(uint32_t j = 0; j < nPipes; j++){
			nChannelsPerPipe[j] += nPerPipeMin + ((n % nPipes) > j);
		}
	}

	uint32_t startIApp = round(8.0 / dt);
	uint32_t endIApp = round(9.0 / dt);
	float amplitude = 30.0;
	float S = 1;

	//int sizeChannels = nChannels * sizeof(float);
	float *vIn = malloc(nCells * sizeof(float));
	int64_t sizeChannelConstants = totalChannels * sizeof(ChannelConstStruct);
	int64_t sizeCellConstants = nCells * sizeof(CellConstStruct);
	ChannelConstStruct *channelConstants = malloc(sizeChannelConstants);
	CellConstStruct *cellConstants = malloc(sizeCellConstants);

	int64_t totalOutChannels = nChannelsPerPipe[0] * nPipes * nSteps;

	int64_t sizeYOut = totalOutChannels * sizeof(float);
	int64_t sizeVOut = (nSteps * nCells) * sizeof(float);

	int64_t addressYOut = sizeChannelConstants + sizeCellConstants + size32Stream;
	int64_t addressVOut = addressYOut + sizeYOut;

	int64_t inputLMemSize = addressYOut;
	int64_t sizeLMemLeft = maxLMemSize - inputLMemSize;

	if(sizeYOut + sizeVOut > sizeLMemLeft){
		throwAwayFactor = 1 + (((sizeYOut + sizeVOut) - 1) / sizeLMemLeft);
		printf("throwAwayFactor is set due to shortage of LMem");
	}


	float *yOut = malloc(sizeYOut);
	float *vOut = malloc(sizeVOut);
	float *yIn = malloc(constantNChannels * sizeof(float));

	// set parameters for each channel/gate
	for(uint32_t i = 0; i < nCells; i++){
		switch(cellType){
		case 0: // HH
			yIn[0] = 0.5;
			yIn[1] = 0.5;
			yIn[2] = 0.5;
			channelConstants[i * constantNChannels] = newChannelConstStruct(0, 1, 25, 0.1, -1, 0, 0.1, 25, 0, 0, \
																			2, 4, 0, 1.0 / 18.0, 0, 0, 0, 0, 1, 0, \
																			3, 0 , vNa, yIn[0]);
			channelConstants[i * constantNChannels + 1] = newChannelConstStruct(2, 0.07, 0, 0.05, 0, 0, 0 ,0, 1, 0,
																				2, 0, 0, 0, 1, 1, 0.1, 30, 1, 0,
																				1, gNa, vNa, yIn[1]);
			channelConstants[i * constantNChannels + 2] = newChannelConstStruct(0, 1, 10, 0.1, -1, 0, 0.01, 10, 0, 0,
																				2, 0.125, 0, 0.0125, 0, 0, 0 ,0 ,1 ,0,
																				4, gK, vK, yIn[2]);
			break;
		case 1: // axon
			yIn[0] = 0.0;
			yIn[1] = 0.9;
			yIn[2] = 0.2369847;
			channelConstants[i * constantNChannels] = newChannelConstStruct(2, 0, 0, 0, 1, 0, 0, 0, 1, 0,
																			14, 0, 0, 0 , 1, 1 ,1.0 / 5.5 , -30, 1, 0,
																			3, 0 , vNaIO, yIn[0]); // m
			channelConstants[i * constantNChannels + 1] = newChannelConstStruct(6, 0, 0, 0, 1 , 1, -1.0 / 5.8, -60, 1, 0,
																				6, 1.5, -40, 1.0 / 33.0, 0, 0, 0 , 0, 1, 0,
																				1, gNa_a, vNaIO, yIn[1]); // h
			channelConstants[i * constantNChannels + 2] = newChannelConstStruct(0, -1, -25, 0.1, 1, 0, -0.13, -25, 0, 0,
																				2, 1.69, -35, 0.0125, 0, 0, 0, 0, 1, 0,
																				4, gK_a, vKIO, yIn[2]); // x
			break;
		case 2: // soma
			yIn[0] = 0.7423;
			yIn[1] = 0.0321349;
			yIn[2] = 0.0;
			yIn[3] = 0.3596066;
			yIn[4] = 0.2369847;
			yIn[5] = 0.1;
			channelConstants[i * constantNChannels] = newChannelConstStruct(6, 0, 0, 0, 1, 1, 1.0/4.2, -61, 1, 0,
																			6, 0, 0, 0, 0, 0, 0 , 0 , 1, 1,
																			3, 0, vCA, yIn[0]); // k
			channelConstants[i * constantNChannels + 1] = newChannelConstStruct(6, 0, 0, 0, 1, 1, -1.0 / 8.5, -85.5, 1, 0,
																				6, 20, -160, -1.0 / 30.0, 0, 1, -1.0 / 7.3, -84, 1, 35,
																				1, gCal, vCA, yIn[1]); // l
			channelConstants[i * constantNChannels + 2] = newChannelConstStruct(2, 0, 0, 0, 1, 0, 0, 0, 1, 0,
																				14, 0, 0, 0, 1, 1 , 1.0 / 5.5, -30, 1, 0,
																				3, 0 , vNaIO, yIn[2]); // m
			channelConstants[i * constantNChannels + 3] = newChannelConstStruct(6, 0, 0, 0, 1, 1, -1.0 / 5.8, -70, 1, 0,
																				6, 3, -40, 1.0 / 33, 0, 0, 0, 0, 1, 0,
																				1, gNa_s, vNaIO, yIn[3]); // h
			channelConstants[i * constantNChannels + 4] = newChannelConstStruct(6, 0, 0, 0, 1, 1, 0.1, -3, 1, 0,
																				6, 47, -50, -1.0 / 900.0, 5, 0 ,0 ,0 ,1, 0,
																				4, gKdr_s, vKIO, yIn[4]); // n
			channelConstants[i * constantNChannels + 5] = newChannelConstStruct(0, -1, -25, 0.1, 1, 0, -0.13, -25, 0, 0,
																				2, 1.69, -35, 0.0125, 0, 0, 0, 0, 1, 0,
																				4, gK_s, vKIO, yIn[5]); // x
			break;
		case 3: // dend
			yIn[0] = 0.0112788;
			yIn[1] = 3.7152;
			yIn[2] = 0.0049291;
			yIn[3] = 0.0337836;
			channelConstants[i * constantNChannels] = newChannelConstStruct(2, 0, 0, 0, 0.34, 1, 1.0 / 13.9, 5, 1, 0,
																				0, -1, -8.5, -0.2, 1, 0, (0.1 * 0.2) / 5.0, -8.5, 0, 0,
																				2, gCaH, vCA, yIn[0]); // r
			channelConstants[i * constantNChannels + 1] = newChannelConstStruct(10, 0, 0, 0, 0, 0, 0, 0, 1, 3,
																				10, 0, 0, 0, 0, 0, 0, 0, 1, 0.075,
																				1, 0, vKIO, yIn[1]); // caconc
			channelConstants[i * constantNChannels + 2] = newChannelConstStruct(3, 0.01, 0, -0.00002, 0, 0, 0, 0, 0, 0,
																				2, 0, 0, 0, 0, 0, 0, 0, 1, 0.015,
																				1, gKCA, vKIO, yIn[2]); // s
			channelConstants[i * constantNChannels + 3] = newChannelConstStruct(6, 0, 0, 0, 1, 1, -0.25, -80, 1, 0,
																			1, 1, -14.6 / 0.086, 0.086, 0, 1, -0.07, 1.87 / 0.07, 0, 1,
																			1, gH, vHIO, yIn[3]); // q
			break;
		}

		// set parameters for each compartment/cell
		switch(cellType){
		case 0: // HH
			vIn[i] = 0;
			cellConstants[i] = newCellConstStruct(startIApp, endIApp, amplitude, S, vL, gL, 0, vIn[i]);
			break;
		case 1: // axon
			vIn[i] = -60;
			cellConstants[i] = newCellConstStruct(startIApp, endIApp, amplitude, S, vL_axon, gL_axon, 0, vIn[i]);
			break;
		case 2: // soma
			vIn[i] = -60;
			cellConstants[i] = newCellConstStruct(startIApp, endIApp, amplitude, S, vL_soma, gL_soma, 0, vIn[i]);
			break;
		case 3: // dend
			vIn[i] = -60;
			cellConstants[i] = newCellConstStruct(startIApp, endIApp, amplitude, S, vL_dend, gL_dend, 1, vIn[i]);
			break;
		}


	}


	// set up data transfer + transfer data to LMEM of DFE
	uint32_t k = 0;
	uint32_t *kPerPipe = malloc(nPipes * sizeof(uint32_t));
	int64_t *sizeChannelsPerPipe = malloc(nPipes * sizeof(int64_t));

	ChannelConstStruct *channelConstantsPerPipe[nPipes];
	
	for(uint32_t i = 0; i < nPipes; i++){
		sizeChannelsPerPipe[i] = nChannelsPerPipe[i] * sizeof(ChannelConstStruct);
		channelConstantsPerPipe[i] = malloc(sizeChannelsPerPipe[i]);
		kPerPipe[i] = 0;
	}

	for(uint32_t i = 0; i < nCells; i++){
		for(uint32_t j = 0; j < nChannels[i]; j++){
			channelConstantsPerPipe[j % nPipes][kPerPipe[j % nPipes]++] = channelConstants[k++];
		}
	}

	max_file_t *maxfile = HH_init();

	int64_t burstSizeInBytes = max_get_burst_size(maxfile, NULL);// 384
	int64_t burstSizeInFloats = burstSizeInBytes / sizeof(float); //  96

	printf("nSteps: %" PRId64 "\n", nSteps);
	printf("nCells: %" PRId64 "\n", nCells);
	printf("totalChannels: %" PRId64 "\n", totalChannels);

	int64_t max = INT64_MAX;

	int64_t addressTemp = 0;
	for(uint32_t i = 0; i < nPipes; i++){
		if(i == 0){
			addressTemp = 0;
		}else{
			addressTemp += sizeChannelsPerPipe[i - 1];
		}
		HH_writeLMem(sizeChannelsPerPipe[i], addressTemp, channelConstantsPerPipe[i]);
	}

	HH_writeLMem(sizeCellConstants, sizeChannelConstants, cellConstants);

	int64_t addressNChannels = sizeChannelConstants + sizeCellConstants;
	HH_writeLMem(size32Stream, addressNChannels, nChannels);
	
	printf("run HH\n");
	gettimeofday(&start, NULL);
	HH(dt, nCells, nSteps, throwAwayFactor, nChannelsPerPipe);
	gettimeofday(&end, NULL);

	printf("read LMem\n");
	HH_readLMem(sizeYOut, addressYOut, yOut);
	HH_readLMem(sizeVOut, addressVOut, vOut);

	printf("writing file\n");
	if(WRITE_OUTPUT){
		FILE *refFile;
		switch(cellType){
		case 0:
			refFile = fopen("../../doc/HHcustomDFE.dat", "w");
			break;
		case 1:
			refFile = fopen("../../doc/HHcustomAxonDFE.dat", "w");
			break;
		case 2:
			refFile = fopen("../../doc/HHcustomSomaDFE.dat", "w");
			break;
		case 3:
			refFile = fopen("../../doc/HHcustomDendDFE.dat", "w");
			break;
		default:
			printf("cellType not supported");
			exit(-1);
			break;
		}
		if(refFile == NULL){
			printf("could not open file, exit");
			exit(-2);
		}
		printf("opened file\n");
		writeInitToFile(refFile, yIn, vIn, dt, nCells, constantNChannels, nSteps);
		writeToFile(refFile, yOut, vOut, dt, nCells, constantNChannels, nSteps);
		fclose(refFile);
	}
	if(WRITE_SINGLE){
		uint32_t cell = 0;
		char buf[0x100];
		switch(cellType){
		case 0:
			snprintf(buf, sizeof(buf), "../../doc/HHcFwd_DFE%d.dat", cell);
			break;
		case 1:
			snprintf(buf, sizeof(buf), "../../doc/HHcFwdAxon_DFE%d.dat", cell);
			break;
		case 2:
			snprintf(buf, sizeof(buf), "../../doc/HHcFwdSoma_DFE%d.dat", cell);
			break;
		case 3:
			snprintf(buf, sizeof(buf), "../../doc/HHcFwdDend_DFE%d.dat", cell);
			break;
		default:
			printf("cellType not supported");
			exit(-1);
		}
		FILE *f = fopen(buf, "w");
		printf("file opened\n");
		writeSingleCell(f, yIn, vIn, yOut, vOut, dt, nCells, constantNChannels, nSteps, cell, nPipes);
		fclose(f);
	}

	printf("duration: ");
	printDuration(&start, &end);
	free(vIn);
	free(yIn);
	free(yOut);
	free(vOut);
	free(channelConstants);
	free(cellConstants);
	free(nChannels);

	printf("finished\n");
	return 0;
}
