package hh;

import com.maxeler.maxcompiler.v2.kernelcompiler.Kernel;
import com.maxeler.maxcompiler.v2.kernelcompiler.KernelParameters;
import com.maxeler.maxcompiler.v2.kernelcompiler.stdlib.LMemCommandStream;
import com.maxeler.maxcompiler.v2.kernelcompiler.stdlib.core.Count;
import com.maxeler.maxcompiler.v2.kernelcompiler.stdlib.core.Count.Counter;
import com.maxeler.maxcompiler.v2.kernelcompiler.types.base.DFEType;
import com.maxeler.maxcompiler.v2.kernelcompiler.types.base.DFEVar;
import com.maxeler.maxcompiler.v2.kernelcompiler.types.composite.DFEStructType;
import com.maxeler.maxcompiler.v2.kernelcompiler.types.composite.DFEStructType.StructFieldType;

public class HHChannelLMemCmd extends Kernel{
	private static final DFEType fpType = dfeFloat(8, 24);
	private static final DFEType uIntType = dfeUInt(32);

	protected HHChannelLMemCmd(KernelParameters parameters, int bufferSize) {
		super(parameters);
		// TODO Auto-generated constructor stub
		StructFieldType aFType = new StructFieldType("aFType", uIntType);
		StructFieldType aX1 = new StructFieldType("aX1", fpType);
		StructFieldType aX2 = new StructFieldType("aX2", fpType);
		StructFieldType aX3 = new StructFieldType("aX3", fpType);

		StructFieldType bFType = new StructFieldType("bFType", uIntType);
		StructFieldType bX1 = new StructFieldType("bX1", fpType);
		StructFieldType bX2 = new StructFieldType("bX2", fpType);
		StructFieldType bX3 = new StructFieldType("bX3", fpType);

		StructFieldType pSFT = new StructFieldType("pSFT", uIntType);
		StructFieldType gSFT = new StructFieldType("gSFT", fpType);
		StructFieldType vChannelSFT = new StructFieldType("vChannelSFT", fpType);
		StructFieldType yInitSFT = new StructFieldType("yInit", fpType);

		DFEStructType channelConstStructType = new DFEStructType(aFType, aX1, aX2, aX3,
																bFType, bX1, bX2, bX3,
																pSFT, gSFT, vChannelSFT, yInitSFT);
		DFEVar prefetchEnable     = control.count.pulse(bufferSize);
		DFEVar burstSize = io.scalarInput("burstSize", dfeUInt(32));
		DFEVar totalChannels = io.scalarInput("totalChannels", dfeUInt(32));
		DFEVar totalChannelBurst = totalChannels * (channelConstStructType.getTotalBits()/Byte.SIZE) / burstSize;

		Count.Params paramsChannelBytes = control.count.makeParams(32)
			.withMax(burstSize)
			.withInc((channelConstStructType.getTotalBits()/Byte.SIZE))
			.withEnable(~prefetchEnable);
		Counter channelByteCounter = control.count.makeCounter(paramsChannelBytes);
		DFEVar channelByteCount = channelByteCounter.getCount();

		Count.Params paramsChannelBurst = control.count.makeParams(32)
			.withMax(totalChannelBurst)
			.withEnable(channelByteCounter.getWrap());
		Counter channelBurstCounter = control.count.makeCounter(paramsChannelBurst);
		DFEVar channelBurstCount = channelBurstCounter.getCount();

		LMemCommandStream.makeKernelOutput("channelCmdStream",
				channelByteCount === 0 & ~prefetchEnable,	// control
				channelBurstCount,                  // address
				constant.var(dfeUInt(8), 1),      	// size
				constant.var(dfeUInt(1), 0),      	// inc
				constant.var(dfeUInt(1), 0),      	// stream
				constant.var(false));
	}


}
