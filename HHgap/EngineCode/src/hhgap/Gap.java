package hhgap;

import com.maxeler.maxcompiler.v2.kernelcompiler.KernelLib;
import com.maxeler.maxcompiler.v2.kernelcompiler.stdlib.KernelMath;
import com.maxeler.maxcompiler.v2.kernelcompiler.stdlib.core.Count;
import com.maxeler.maxcompiler.v2.kernelcompiler.stdlib.core.Count.Counter;
import com.maxeler.maxcompiler.v2.kernelcompiler.stdlib.core.Stream.OffsetExpr;
import com.maxeler.maxcompiler.v2.kernelcompiler.stdlib.memory.Memory;
import com.maxeler.maxcompiler.v2.kernelcompiler.types.base.DFEType;
import com.maxeler.maxcompiler.v2.kernelcompiler.types.base.DFEVar;
import com.maxeler.maxcompiler.v2.kernelcompiler.types.composite.DFEVector;
import com.maxeler.maxcompiler.v2.kernelcompiler.types.composite.DFEVectorType;
import com.maxeler.maxcompiler.v2.utils.MathUtils;


public class Gap extends KernelLib{
	private DFEVar IC;
	private final DFEVar cellCount;
	private final Memory<DFEVar> ICMem;
	private final DFEVar outEnable;
	private final DFEVar iterationCount;
	private final DFEVar start;
	private final int unrollFactor;
	private final int nCellsMax;
	private final DFEVar enable;
	private final DFEVar nCells;
	private final DFEVar totalChannels;
	private final DFEType fpType;

	Gap(KernelLib owner, DFEVar nCells, DFEVar enable, DFEVar totalChannels, int unrollFactor, int nCellsMax, DFEType fpType){
		super(owner);
		Count.Params paramsCell = control.count.makeParams(32)
			.withMax(nCells)
			.withEnable(enable)
			.withInc(1);
		Counter cellCounter = control.count.makeCounter(paramsCell);
		Count.Params paramsIterations = control.count.makeParams(32)
			.withMax(nCells / unrollFactor)
			.withInc(1)
			.withEnable(cellCounter.getWrap());
		Counter iterationCounter = control.count.makeCounter(paramsIterations);


		this.iterationCount = iterationCounter.getCount();
		this.cellCount = cellCounter.getCount();
		this.start = this.iterationCount === 0 & this.cellCount === 0;
		this.unrollFactor = unrollFactor;
		this.nCellsMax = nCellsMax;
		this.outEnable = (iterationCount === (nCells  / unrollFactor) - 1); // Enable on final iteration
		this.ICMem = mem.alloc(fpType, nCellsMax);
		this.enable = enable;
		this.totalChannels = totalChannels;
		this.nCells = nCells;
		this.fpType = fpType;
	}

	public void calculate(Memory<DFEVar> vMem, DFEVector<DFEVar> w, DFEVector<DFEVar> gxs){
		DFEVectorType<DFEVar> vectorType =
			new DFEVectorType<DFEVar>(this.fpType, this.unrollFactor);
		DFEVector<DFEVar> ICUpVector = vectorType.newInstance(this);
		OffsetExpr gapOffset = stream.makeOffsetAutoLoop("gapOffset");

		DFEVar addressOwn = cellCount.cast(dfeUInt(MathUtils.bitsToAddress(nCellsMax)));
		DFEVar ICOld = iterationCount === 0 ? constant.var(this.fpType, 0) : this.ICMem.read(addressOwn);

		DFEVar vOwn = vMem.read(addressOwn);
		for(int i = 0; i < this.unrollFactor; i++){;
			DFEVar addressOther = (iterationCount * unrollFactor + i).cast(dfeUInt(MathUtils.bitsToAddress(nCellsMax)));
			DFEVar vOther = vMem.read(addressOther);
			DFEVar vDiff = vOwn - vOther;
			DFEVar expTemp = KernelMath.abs(vDiff) > constant.var(fpType, 255.0) ? constant.var(fpType, 0.0) : KernelMath.exp(-0.01 * vDiff * vDiff);
			ICUpVector[i] <== w[i] * (gxs[0] * expTemp + gxs[2]) * vDiff;
		}


		this.IC = ICOld + Reductions.reduce(ICUpVector.getElementsAsList());
		this.ICMem.write(stream.offset(addressOwn, -gapOffset + 3),
				stream.offset(this.IC, -gapOffset + 3),
				stream.offset(this.enable, -gapOffset + 3));
    }

	public DFEVar getIC(){
		return IC;
	}

	public DFEVar waitOnGap(){
		return (iterationCount * nCells + cellCount) >= totalChannels;
	}

	public DFEVar getOutEnable(){
		return outEnable;
	}

	public DFEVar getIterationCount(){
		return iterationCount;
	}

	public DFEVar getCellCount(){
		return cellCount;
	}

	public DFEVar newStep(){
		return start;
	}

	public void simWatch(){
		cellCount.simWatch("gapIterationCount");
		iterationCount.simWatch("gapIterationCount");
	}
}
