package hhgap;


import java.util.List;

import com.maxeler.maxcompiler.v2.kernelcompiler.types.base.DFEVar;
import com.maxeler.maxcompiler.v2.kernelcompiler.types.composite.DFEVector;

public class Reductions {
    public static DFEVar reduce(DFEVector<DFEVar> vector) {
      return reduce(vector.getElementsAsList());
    }

    public static DFEVar reduce(List<DFEVar> vector) {
      if (vector.size() == 1)
        return vector.get(0);
      return
        reduce(vector.subList(0, vector.size() / 2)) +
        reduce(vector.subList(vector.size() / 2, vector.size()));
    }

}
