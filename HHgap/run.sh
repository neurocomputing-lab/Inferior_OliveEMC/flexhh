#!/bin/sh

# name
#$ -N lr_run
# shell
#$ -S /bin/sh

# execute script from the current directory
#$ -cwd

# set up the necessary environment
export LD_LIBRARY_PATH=/panfs/panfs.maxeler/packages/mpcx_power:${LD_LIBRARY_PATH}
source /panfs/panfs.maxeler/maxeler/maxcompiler-2016.1.1/settings.sh
export PATH=/panfs/panfs.maxeler/maxeler/altera/13.1/quartus/bin:${PATH}
export MAXELEROSDIR=/opt/maxeler/maxeleros

# run the code
pushd CPUCode
maxorch -r 192.168.74.198 -c reserve -i niallgap24 -t  "MAIA"
SLIC_CONF="default_engine_resource=niallgap24^192.168.74.198" ./HHgap
maxorch -r 192.168.74.198 -c unreserve -i niallgap24
popd
